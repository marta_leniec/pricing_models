%let USERID=FFBJ02;
%let MY_PASSWORD=sep2015;
%let mvs=10.240.14.1 5023;
options remote=mvs comamid=tcp;
filename rlink "/sasdata/gemensam/folksam2.scr";
signon;

rsubmit;
filename H50  'H90.H50RECT1.M1506';
DATA BEST;
      INFILE H50;
		INPUT 
			�    1   ID     �6.      
			�    7   SKA     4.      
			�    7   GEN     6.      
			�    22  KTD    �8.      
			�    30  PNR12  �12.      
			�    32  PNR    �10.      
			�    42  KON    �1.      
			�    43  FNR    �9.      
			�    52  REG    �6.      
			�    58  PROD   �3.      
			�    66  FKL    �2.      
			�    68  FBO     8.      
			�    68  FBONR  YYMMDD8.
			�    76  ANN     8. 
			�    84  FST    �1. 
			�    86  ATYP   �2. 
			�    88  FTDAR   4. 
			�    88  FTD     �8. 
			�    96  HFD     8. 
			�   100  HFM     2. 
			�   104  TOND   �8. 
			�   148  TRM    �1.
			�   149  BETT   �1.  
			�   152  LKF    �6. 
			�   152  LAN    �2. 
			�   154  KOM    �2. 
			�   180   FK    �4. 
			�   184   MO     2. 
			�   184  OMR     2.
			�   186  OMB     �5.
			�   166  PON    �5.
			�   201  KAMP1  �4.
			�   205  KAMP2  �4.
			�   209  KAMP3  �4.
			�   213  KAMP4  �4.
			�   217  MEDL1  �4.
			�   221  MEDL2  �4.
			�   225  MEDL3  �4.
			�   229  MEDL4  �4.
			�   237  APT     7.
			�   244  APGT    7.
			�   251  APD     7.
			�   258  APGD    7. 
			�   265  APV     7. 
			�   272  APGV    7. 
			�   279  APHYR    7. 
			�   286  APASS   7. 
			�   286  APASS   7. 
			�   293  APANSV  7.
			�	300	 APBX	 7.
			�   385  SRTRAF  5. 
			�   390  SRBRAND 5. 
			�   395  SRSTOLD 5. 
			�   400  SRRADDN 5. 
			�   405  SRRATTS 5. 
			�   410  SRGLAS  5. 
			�   415  SRMASK  5.
			�   420  SRVAGN  5. 
			�   425  SRHYRB  5. 
			�   430  SRASS   5. 
			�   435  SRANSV  5. 
			�   440  FOF    �3. 
			�   440  FOFT   �1. 
			�   441  FOFD   �1. 
			�   442  FOFV   �1. 
			�   454  HYR    �1. 
			�   455  ASS    �1.
			�   592  VX 	�1.   /* till�gg volvo*/
			�   594  BX 	�1.   /* till�gg �vriga, ej partner*/
			�   457  BMKOD  �6. 
			�   457  BMKOD3 �3. 
			�   475  AKL     2. 
			�   477  ALD     3.
			�   480  PKL6   �6.
			�   480  PKLT   �2.
			�   482  PKLD   �2.
			�   484  PKLV   �2.
			�   484  PKL2   �2.
			�   485  PKL    �1.
			�   486  TNIV   �2.
			�   488  DNIV   �2.
			�   490  VNIV   �2.
			�   487  ZON    �1.
			�   492  KST    �1.
			�   493  BR     �1.
			�   494  BKT    �2.
			�   496  SFA     2. 
			�   502  BKV    �2.
			�   513  PARK   �1.
			�   514  ANVSAT �1.
			�   515  ANTFOU �1.
			�   517  FSSUTR �1.
			�   518  FELUTT �1.
			�   519  FELUTV �1.
			�   520  OVRUTT �1.
			�   521  OVRUTV �4.
			�   533  UHD    �8.
			�   541  FRD    �8.
			�   541  MDA    �4.
			�   563  DINP   �1.
			�   565  FAB    �4.
			�	574 TOTVIKT �5.
			�   583  EFF    �4.  
			�   587  EFFT   �4.  
			�   616  NIVT   �2.  
			�   618  NIVD   �2.  
			�   620  NIVV   �2.  
			�   622  NYPKLT �1.  
			�   623  NYPKLD �2.  
			�   625  NYPKLV �2.  
			�   640  TOTKREDV �1.
			�   641  BETANM �1.  
			�   642  BETDAT  8.  
			�   650  KKUTFD  8.  
			�   658  KKKLS �13.  
			�   671 KKALD   3.
			�	723	Innehav �9. 
			�  ;
            
APTUS=APT; 
            *IF FBO<= &SLUT AND ANN=99999999 THEN DELETE;
                                                                      
            IF ANN=99999999 THEN GOTO HOPP;                           
              INPUT  �    76  ANNNR  YYMMDD8.;                        
            HOPP:                                             
   RUN;
 proc download data=best out=ffbj02.h50skad; run;
endrsubmit;