/***************************************************************************************************************
/* 	�ndrad av  		Datum     		Kommentar                                             
 	======   		=====     		=========                                             
	David Sj�gren	2017-02-23		Skapar programmet, som tar ut data fr�n Y75 (sg70ana.skadestatistik_y75_agr_manad)
									f�r motor, samt definierar uppdelningar f�r moment, produkt, samt 
									Utb, Res, Srb, etc.

	David Sj�gren	2018-05-07		Skriver om Grundprogrammet f�r Stj�rnvyerna (SKADEKOMPONENT och BETALNINGSRAD). 
									Inget �vrigt f�r�ndrat fr�n gamla grundprogrmmet (som gick mot DET/AGR).

									Syfte: 
									att samma definitioner av dessa alltid anv�nds, 
									att man ska kunna �ndra definitioner/r�tt upp felaktigheter i data centralt,
									att slippa skriva om samma grund-kod i varje program.

	Per Sj�din 		2018-09-18		Lag till ett datasteg f�r att flytta konverterade l�tta lastbilar fr�n P-bil till �bil.						

	David Sj�gren	2018-11-12		1.	Lagt till kod som flyttar Hyrbils- o Assistans-utbetalningarna i H50 
										fr�n ursprungliga momenten till egna moment f�r Pbil o �bil, 
										dvs det vi tidigare gjorde i H50rect1- och lakan-programmen
									2.	Begr�nsat variabler som tas ut fr�n BET f�r att f�rb�ttra k�rningstiden
									3.	Hanterat Pbils H50-regresser (separat, eftersom de hanteras separat sedan tidigare)

	Stefan Maras	2018-11-26		Skriver om s� att vi sorterarar ut allt data map PRODUKT f�rst f�r att korta ner k�rtiden

	Per Sj�din 		2018-12-07		Lagt till moment i Assistanshanteringen f�r att undvika dubbelr�kning.

	Kiat Wee Wong	2019-07-01		Lagt in Riddermark, KTGKOMB = RMB och RMF
	
/***************************************************************************************************************

Instruktioner:
F�r att f� motordata fr�n Stj�rnan, skriv:

%include "&Winshares/HKAKTUARIEC/Prognos/Motor/Folksam/Databas/Program/Grundprogram/Stjarnan/Grundprogram_Stjarnan.sas";
%STJARNAN(ARMAN = yyyymm, PRODUKT = En_produkt, UTFIL = En_utfil);

d�r 
yyyymm = datumet f�r vilket data h�mtas
En_produkt = (PBIL / VSG / OBIL / TA)
En_utfil = namnet p� utfilen, tex mylib.mytable 

Exempel:
%include "&Winshares/HKAKTUARIEC/Arbetsrum/Motor/Grundprogram/STJARNAN/Grundprogram_Stjarnan.sas";
%STJARNAN (ARMAN = 201804, PRODUKT = PBIL, UTFIL = mylib.mytable);


/***************************************************************************************************************/

%MACRO STJARNAN (ARMAN, PRODUKT, UTFIL);

	/* Sl�r p� logg-texter f�r makron (f�r programlogik, makro-statements och -variabler) */
	options mlogic;
	options symbolgen;

	/* Omvandlar prod-variabeln */
	%if 	  &PRODUKT=PBIL 			%then %let PROD1_VAR=1;
	%else %if &PRODUKT=VSG 				%then %let PROD1_VAR=2; 
	%else %if &PRODUKT=OBIL 			%then %let PROD1_VAR=3;
	%else %if &PRODUKT=TA 				%then %let PROD1_VAR=4;
	%else 								 	  %abort; /* Om man l�gger in felaktig PRODUKT s� avbryts programmet */

	/* Definition av produkt-variabler */
	%LET PBILPROD = 'FDP','FPB','HGP','HPB','HYP','KIA','LIB','LPB','MAZ','MIT', 'OBU', 'RMB', 'RMF',
					'OKB','OPB','SAA','SPB','TIB','TPB','VPB','VWF','SKO','PEG', 'KPB', 'KLB';
	%LET VSGPROD = 	'GAR';
	%LET TAPROD = 	/*'STA', 'LTA', 'TTA', 'HTA', 'MTA'*/ 'ASS';
	%LET OBILPROD = 'ELR', 'FLB', 'HBL', 'HGL', 'HSV', 'MCA', 'MCL', 'MCT', 'MHR', 'MPE', 'MPR', 'SLV', 'SMC', 'TLB', 'TRA', 
					'TRS', 'TSA', 'OHB', 'OKS', 'KLM', 'KMR', 'KSV', 'KTK', 'KTM','KHV','KTS','KLB','ELR',
					'MBU', 'MHV', 'MPB', 'MTL', 'MLL', 'KHB', 'MTR', 'MMC', 'MMO';
	%PUT _USER_;


	/* Tar ut aktuellt manadid */ 
	data h1 (keep= antal_betalrader antal_doda antal_motpart antal_skadade antal_skadekomponentrader arsmodell 
				automatreglerad_flagga avtalsnr belopp_sek belopp_sek_sjvrisk belopp_sek_skadereserv_beraknat 
				belopp_sek_skadereserv_y75 belopp_sek_utbetald bilmkod fkl fln fnrt forsavtnr_dd forsavtnr_kpl
				handelsetyp kallsystem kategori ktgkomb_avtal ktgkomb kundnr_kund manadid motpart_pnr omfattn
				omfattn_motor_avtal pnr_kund prodgrp_avtal prodgrp_kortnamn_avtal regnr regress_flagga
				skadeanmalan_avslutad_flagga skadearende_dd skadearende_k60_nummer /*skadearende_legacy_nummer*/
				skadearende_legacy_lopnr skadearende_nummer skadedatum skademoment_beskr skademoment_dd
				skademoment_k60_beskr skademoment_k60_kod skademoment_kod skadereservdatum skapaddatum
				sln slutdatum stangningorsak_kod stangningsorsak_beskr systemanmalan_flagga teckningsdat_avtal
				vallande verkomr_avtal webbanmalan_flagga regnr);

		retain forsavtnr_dd skadearende_nummer skademoment_dd;

		set sg70skad.fakta_vy_skadekomponent_hist; 
		where manadid  = &arman and skadeomrade_kod in ('PSS','Motor');

	run;

	/* Definition av Prod1, Res, Srb. Uppm�rkning MHRF. */
	data h1_temp;
		set h1;
		if prodgrp_avtal='MHR' 										then ktgkomb='MHR';
		if ktgkomb in (&pbilprod) then do;
			if kategori in ('LLF' 'LLJ') 							then Prod1=3; /*Flyttar konveterade l�tta lastbilar till �-bil*/
			else 													Prod1=1;
		end;

		else if ktgkomb in (&vsgprod)								then prod1=2; 
		else if ktgkomb in (&obilprod)								then prod1=3; 
		else if ktgkomb in (&taprod) 								then prod1=4; 
		else 														 Prod1=999; 
	run;

	/* Tar ut aktuella produkter (Pbil / �bil / ... ) */
	data h1_prod;
		set h1_temp;
		where prod1 = &prod1_var;
	run;

	/* Definition av moment (Mom och Smo), Ska och Ant. */
	data h2;
		format mom $2.;
		set h1_prod;

		/* TRAFIK-MOMENT */
		 	 if skademoment_DD = 'MTP' then do; 							Mom = 'TP'; Smo = '01'; 	end;
		else if skademoment_DD in ('MT' 'MP') then do; 						Mom = 'TE'; Smo = '02'; 	end;

		/* DELKASKO-MOMENT */
		else if skademoment_DD = 'MB' then do; 								Mom = 'B'; 	Smo = '03'; 	end; 
		else if skademoment_DD = 'MG' then do; 								Mom = 'G'; 	Smo = '04'; 	end;
		else if skademoment_DD = 'MJ' then do; 								Mom = 'J'; 	Smo = '05'; 	end;
		else if skademoment_DD = 'MM' then do; 								Mom = 'M'; 	Smo = '06'; 	end;
		else if skademoment_DD = 'MDM' then do; 							Mom = 'OV'; Smo = '99'; 	end; /*husvagn/husbil skador - inredning, elektronik*/
		else if skademoment_DD = 'MR' then do; 								Mom = 'R'; 	Smo = '07'; 	end;
		else if skademoment_DD = 'MFA' then do; 							Mom = 'A'; 	Smo = '07'; 	end;
		else if skademoment_DD = 'MSE' then do; 							Mom = 'R'; 	Smo = '07'; 	end; /* Sj�lvriskeliminering R�ddning*/
		else if skademoment_DD = 'MSR' then do; 							Mom = 'R'; 	Smo = '07'; 	end; /* Sj�lvriskreducering  R�ddning*/
		else if skademoment_DD = 'MS' then do; 								Mom = 'S'; 	Smo = '08'; 	end;

		/* VAGN-MOMENT */
		else if skademoment_DD = 'MV' then do; 								Mom = 'V'; 	Smo = '09'; 	end;
		else if skademoment_DD = 'MVG' then do; 							Mom = 'V'; 	Smo = '09'; 	end;
		else if skademoment_DD = 'MBV' then do; 							Mom = 'V'; 	Smo = '09'; 	end;

		/* �VRIGA MOMENT */
		else if skademoment_DD in ('MBX' 'MFT' 'MKU' 'MNY' 'MHY')  
			then do; 														Mom = 'TL'; Smo = '10';		end;
		else if skademoment_DD = 'MK' then do; 								Mom = 'OV'; Smo = '10'; 	end;/* Kris l�ggs tempor�rt h�r pga �bil? */

		/* TA-MOMENT */
		else if skademoment_DD in ('MAG' 'MGA') then do; 					Mom = 'AS'; /*Smo = '';*/ 	end;

		/* FELHANTERING */
		else do; 															Mom = '??';	Smo = 'ER'; 	
		/*																	Put "ERROR: Unders�k om " skademoment " �r ett nytt skademoment i Y75";
		*/																								end;

		ska= put(year(skadedatum),4.);
		ant=1;
	run;



	/* START, kod som flyttar Hyrbils- och Assistans-utbetalningar i H50 till egna moment-rader.*/
	/**************************************************************************************************************/

	%if &PRODUKT=PBIL or &PRODUKT=OBIL %then %do;

		/* H�mtar Assistans- och Hyrbils -utbetalningarna i H50, fr�n BET */
		/* L�gger upp dessa i separata kolumner */

		data till1;
			set sg70skad.fakta_vy_skadebetalningsrad_hist /*!!*/
				(keep = skadeomrade_kod manadid ktgkomb skadearende_dd skadearende_legacy_nummer 
						skadearende_legacy_lopnr  skademoment_dd sec1_code sec2_code sec1_desc sec2_desc 
						betalningsdatum belopp_sek_utbetald kallsystem); 
			where SKADEOMRADE_KOD IN ('PSS','Motor') and  manadid=&ARMAN.;
		run;

		data till2;
			set till1 ;
			where sec1_code in ('AS','E2');

			if sec1_code = 'AS' then bel1 = belopp_sek_utbetald;
			      else bel1 = 0;
			if sec1_code = 'E2' then bel2 = belopp_sek_utbetald ;
			      else bel2 = 0;
			if bel1 = 0 and bel2 = 0 then delete;
		run;   

		/* Sorterar o summerar per skadenr och l�pnr */
		proc sort data=till2 out=till3;
		  by skadearende_legacy_nummer skadearende_legacy_lopnr;
		run;

		proc summary data=till3 missing nway;
		   by skadearende_legacy_nummer skadearende_legacy_lopnr;
		   var bel1 bel2 ;
		   output out=till4 sum=;
		run;

		/* Matchar p� utbetalningarna p� mastertabellen */
		proc sql feedback;
		  create table till5 as
			select a.*, 
				b.bel1 as utbetalat_assistans, b.bel2 as utbetalat_hyrbil
			from h2 a left join till4 b
			on a.skadearende_k60_nummer=b.skadearende_legacy_nummer and a.skadearende_legacy_lopnr=b.skadearende_legacy_lopnr
			;
		quit;

		/*F�r alla moment f�rutom Assistans, Hyrbil: Utbetalningar f�r Assistans och Hyrbil subtraheras fr�n BELOPP_SEK_UTBETALD, 
		 dvs BELOPP_SEK_UTBETALD �r d�refter exklusive utbetalningar f�r Assistans o Hyrbil. 
		 Assistans- och hyrbils-utbetalningarna l�ggs upp p� egna momentrader.
		*/
		data till6;
			set till5;

			if skademoment_dd not in ('MAS','MHY', 'MFA') then 
				belopp_sek_utbetald= sum(belopp_sek_utbetald,-utbetalat_assistans,-utbetalat_hyrbil); 
			output;

			if skademoment_dd not in ('MAS', 'MHY', 'MFA') then do;
				if utbetalat_assistans not in (0, .) then do;
					Mom = 'A'; 	
					Smo = '07';
					belopp_sek_utbetald=utbetalat_assistans;
					Ant=1;
					Temp=utbetalat_hyrbil;
					utbetalat_assistans=0;
					utbetalat_hyrbil=0;
					output;
					utbetalat_hyrbil=temp;
				end;
				if utbetalat_hyrbil not in (0, .) then do;
					Mom = 'TL'; 
					Smo = '10';
					belopp_sek_utbetald=utbetalat_hyrbil;
					Ant=1;
					utbetalat_assistans=0;
					utbetalat_hyrbil=0;
					output;
				end;
			end;	
		run;

		/* Snabbl�sning */
		data h2;
			set till6;
		run;

	%end;

	/* SLUT, kod som flyttar Hyrbils- och Assistans-utbetalningar i H50 till egna moment-rader.*/
	/**************************************************************************************************************/




	/* 
	Justering f�r antalsr�kning f�r Trafik/Egendom och Trafik/Person.

	En skadeh�ndelse kan ha flera skadelidande f�r TP, d� l�ggs en rad upp per skadelidande.
	Genom justeringen nedan sker antalsr�kningen ske p� samma s�tt som i H50, dvs
	�ven om det finns flera TE/TP -moment f�r en skadeh�ndelse, s� r�knas de �nd� bara som en skada, dvs 
	ANT s�tts till 1 f�r en av raderna och 0 f�r �vriga rader.
	Resultatet av detta �r allts� att vi r�knar "Antal skadeh�ndelser" f�r TP, inte antal skadelidande (dvs precis som vi r�knar i H50).
	*/

	proc sort data=h2 out=h21; 
		by forsavtnr_dd skadedatum regnr /*h�r ska v�r definition av "skadeh�ndelse" ligga 
		(f�rs�kringsavtals-numret, registreringsnumret och skadedatumet enligt Caroline S:s mail)*/
		smo;
	run;

	data h22; 
		set h21;
		by forsavtnr_dd skadedatum regnr smo;
		if SMO in ('01', '02') then do;
			if first.smo then ant=1;
			else ANT=0;
		end;
	run;

	/* Snabbl�sning s� att TP ej r�knas dubbelt g�llande antal skador, f�r K60-skadorna som �ven finns i Y75. 
	(Det g�r ej att koppla skadorna i Y75 till K60 i dagsl�get.) */

	data h23;
		set h22;
		if (Smo = '01' and ska<=2003 and substr(forsavtnr_DD,1,3)='Y75') then ant=0;
	run;


	/* H�r hanteras k�nda felaktigheter i indatat */
	data h5;
		set h23;
		if ska=1900 then delete; /* En tillf�llig l�sning f�r att hantera tv� skador som har skade�r 1900, tror att detta �r testdata. Kolla, rensa ev bort */
		sse = belopp_sek_utbetald;
		srb = belopp_sek_skadereserv_y75; /*belopp_sek_skadereserv_beraknat!*/
		srr = belopp_sek_skadereserv_y75-belopp_sek_utbetald; /* srr=skadereglerarreserv */
	run;


	/*********************************************************************************************************/
	/* Vagnregresser Pbil */

	%if &PRODUKT=PBIL  %then %do;

	/* Tar ut 
	aktuellt MANADID,
	alla Pbil-produkter,
	Vagn-momentet (exkl VSG),
	regress-inbetalningskoderna
	*/
	data r1;
	set sg70skad.fakta_vy_skadebetalningsrad_hist /*!!*/
		(keep = skadeomrade_kod manadid ktgkomb skademoment_dd ersattningstyp_kod sec1_code sec2_code
				skadearende_dd forsavtnr_dd belopp_sek_utbetald); 

		where skadeomrade_kod in ('PSS','Motor') and
			manadid=&ARMAN. and KTGKOMB in (&PBILPROD.) and skademoment_DD in ('MV', 'MBV') 
			and (ersattningstyp_kod in ('CC10137', 'CC10151', 'CC10152') or SEC1_CODE in ('EK') or SEC2_CODE in ('EK')); 
	run;

	/* Summerar regresserna per skadenr */
	proc sql feedback;
		create table r2 as
		select SKADEARENDE_DD, 'V' as mom, /* eftersom "skademoment in ('MV', 'MBV')" s� �r alltid mom='V' h�r. */
				forsavtnr_dd, 
				sum(belopp_sek_utbetald) as total_regress
		from r1
		group by skadearende_dd, mom, forsavtnr_dd;
	quit;

	/* Matchar vagnregresserna p� skadetabellen mha skadenr. Obs att det finns en annan tabell som ocks� heter h5 ovan. */
	proc sql feedback;
	  create table r3 as 
		select a.*, coalesce(b.total_regress, 0) as SSE3
		from h5 a left join r2 b
		on a.SKADEARENDE_DD=b.SKADEARENDE_DD and a.mom=b.mom and a.FORSAVTNR_DD=b.FORSAVTNR_DD
	;
	quit;

	/* Snabbl�sning f�r slippa skriva makron f�r �bil o VSG. */
	data h5;
		set r3;
	run;

	%end;

	/* Vagnregresser Pbil, slut*/
	/*********************************************************************************************************/






	/* Felhantering*/

	/* H�r skrivs alla rader med Prod1=999 ut */
	data ERROR_Prod1_ej_uppmarkt;
		set h5;
		if Prod1=999;
		if KTGKOMB^='OBU'; /* OBU f�r Prod1=999, men de �r inte "�kta" felrader eftersom de hanteras i en annan rutin. */
	run;

	/* H�r skrivs alla rader med Smo='ER' ut */
	data ERROR_Smo_ej_uppmarkt;
		set h22;
		if Smo='ER';
		if skademoment_DD^='MBA'; /* OBU f�r Prod1=999, men de �r inte "�kta" felrader eftersom de hanteras i en annan rutin. */
	run;

	/* Sorterar */
	proc sql;
	  create table h6 as
		select *
		from h5
		order by skadedatum, skadearende_dd;
	quit;

	/* Skapar utfil */
	data &utfil;
		retain forsavtnr_dd skadearende_dd skademoment_dd mom smo prodgrp_avtal ktgkomb skadedatum sse sse3 srr srb ant;
	set h6;

	run;

	/* Sl�r av makro-loggtexten */
	options nomlogic nosymbolgen;

%MEND; /* STJARNAN */

/********************** SLUT Grundprogram   ************************************* ************************************* */



 



